const functions = require("firebase-functions");

exports.social = functions.https.onRequest((req, res) => {
  console.log(
    "social",
    req.headers["user-agent"],
    req.headers["user-agent"].includes("facebookexternalhit"),
    req.query
  );
  const redirect = `https://rinhbiavenha.com?source=${req.query["source"]}`;
  if (req.headers["user-agent"].includes("facebookexternalhit")) {
    res.status(200).send(`
    <html>
        <head>
            <title>Heineken - Rinh bia về nhà. Tiền nạp thả ga.</title>
            <meta  name="description" content="Rinh bia về nhà. Tiền nạp thả ga.">
            <!-- You can use Open Graph tags to customize link previews.
            Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
            <meta  property="og:type" content="website">
            <meta  property="og:title" content="Heineken - Rinh bia về nhà. Tiền nạp thả ga.">
            <meta  property="og:description" content="${req.query["source"]}">
            <meta  property="og:image" content="https://i.imgur.com/r6CISD9.png">
        </head>
        <body></body>
    </html>
    `);
  } else {
    res.redirect(redirect);
  }
});
