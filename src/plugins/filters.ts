import  Vue  from  "vue";
import format from 'date-fns/format'
import vi from 'date-fns/locale/vi'
import { parseISO } from "date-fns";

Vue.filter("currency", (value: any) => {
    let price
    if (typeof value == 'string') {
        price = Number(value)
        if (isNaN(price)) return value
    } else if (typeof value == 'number') {
        price = value
    } else {
        return value
    }
    
    var formatter = new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND',
        minimumFractionDigits: 0
    });
    return formatter.format(price);
})

Vue.filter("date", (value: Date | string, formatString: string = 'dd/MM/yyyy') => {
    let date = typeof(value)=='string'?parseISO(value):value
    return format(date, formatString, {
        locale: vi
    })
})

Vue.filter("timeSlot", (value: number) => {
    let start = Number(value)
    let end = start + 1
    return `${(start<10)?'0':''}${start}:00 - ${(end<10)?'0':''}${end}:00`
})